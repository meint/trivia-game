<?php

require_once('Player.php');


function echoln($string)
{
    echo $string . "\n";
}

/**
 * A multiplayer trivia game for command line purpose
 */
class Game
{
    private $players;

    private $questions;

    private $currentPlayer = 0;
    private $isGettingOutOfPenaltyBox;

    const QUESTION_TYPE_POP = "pop";
    const QUESTION_TYPE_SCIENCE = "science";
    const QUESTION_TYPE_SPORTS = "sports";
    const QUESTION_TYPE_ROCK = "rock";

    public function __construct()
    {
        $this->players = [];
        $this->questions = $this->generateQuestions();
    }

    /**
     * Create 50 questions for each question type
     *
     * @return array with questions
     */
    private function generateQuestions()
    {
        // Create 2d array of categories
        $questions = [
            self::QUESTION_TYPE_POP => [],
            self::QUESTION_TYPE_SCIENCE => [],
            self::QUESTION_TYPE_SPORTS => [],
            self::QUESTION_TYPE_ROCK => []
        ];

        // add questions
        for ($i = 0; $i < 50; $i++) {

            $questions[self::QUESTION_TYPE_POP][] = "Pop Question " . $i;
            $questions[self::QUESTION_TYPE_SCIENCE][] = "Science Question " . $i;
            $questions[self::QUESTION_TYPE_SPORTS][] = "Sports Question " . $i;
            $questions[self::QUESTION_TYPE_ROCK][] = "Rock Question " . $i;
        }
        return $questions;
    }

    /**
     * Check if the game is playable
     *
     * @return boolean
     */
    public function isPlayable(): bool
    {
        return ($this->numPlayers() >= 2);
    }


    /**
     * Add a player
     *
     * @param string $playerName
     * @return int new number of players
     */
    function add($playerName): int
    {
        // Create new player
        $this->players[] = new Player($playerName);

        echoln($playerName . " was added at position " . count($this->players));
        return $this->numPlayers();
    }


    /**
     * Get number of players
     *
     * @return int
     */
    private function numPlayers(): int
    {
        return count($this->players);
    }

    /**
     * Roll the dice
     *
     * @param int $roll
     * @return void
     */
    function roll($roll)
    {
        $player = $this->players[$this->currentPlayer];
        $rollIsZero = $roll % 2 == 0;

        echoln($player->name . " is the current player");
        echoln("They have rolled a " . $roll);

        if ($player->inPenaltyBox && $rollIsZero) {
            echoln($player->name . " is not getting out of the penalty box");
            $this->isGettingOutOfPenaltyBox = false;
            return;
        }

        // Check if player can come out of penalty box
        if ($player->inPenaltyBox) {
            $this->isGettingOutOfPenaltyBox = true;

            echoln($player->name . " is getting out of the penalty box");
        }

        // Raise position
        $player->place += $roll;
        $player->place = $player->place % 12;

        // Report and go to next question
        echoln($player->name . "'s new location is $player->place");
        echoln("The category is " . $this->currentCategory());
        $this->askQuestion();
    }

    /**
     * Ask the question for the current player
     *
     * @return void
     */
    function askQuestion()
    {
        $category = $this->currentCategory();
        $question = array_shift($this->questions[$category]);
        echoln($question);
    }


    /**
     * Get category based on current player
     *
     * @return enum of QUESTION_TYPE
     */
    private function currentCategory()
    {
        $player = $this->players[$this->currentPlayer];
        $mod = $player->place % 4;
        switch ($mod) {
            case 0:
                return self::QUESTION_TYPE_POP;
            case 1:
                return self::QUESTION_TYPE_SCIENCE;
            case 2:
                return self::QUESTION_TYPE_SPORTS;
            case 3:
            default:
                return self::QUESTION_TYPE_ROCK;
        }
    }


    /**
     * Report when player answered correctly
     *
     * @return void
     */
    public function wasCorrectlyAnswered(): void
    {

        $player = $this->players[$this->currentPlayer];

        if ($player->inPenaltyBox && !$this->isGettingOutOfPenaltyBox) {
            $this->nextPlayer();
            return;
        }


        echoln("Answer was correct!!!!");
        $player->purse++;
        echoln("$player->name now has $player->purse Gold Coins.");


        if ($this->didPlayerWin()) {
            echoln("================ The winner is ==================");
            echoln("============== $player->name ");
            return;
        }
        $this->nextPlayer();
    }

    /**
     * Report when the palyer answerred incorrect
     *
     * @return boolean
     */
    public function wrongAnswer(): bool
    {
        $player = $this->players[$this->currentPlayer];

        echoln("Question was incorrectly answered");
        echoln($player->name . " was sent to the penalty box");

        $this->setCurrentPlayerPenaltyBox(true);

        $this->nextPlayer();
        return true;
    }

    /**
     * Go to the next possible player
     *
     * @return void
     */
    private function nextPlayer()
    {
        $this->currentPlayer++;
        $this->currentPlayer = $this->currentPlayer  % count($this->players);
    }

    /**
     * Put the current player in the penalty box or not
     *
     * @param boolean $state
     * @return void
     */
    private function setCurrentPlayerPenaltyBox(bool $state): void
    {
        $player = $this->players[$this->currentPlayer];
        $player->inPenaltyBox = $state;
    }


    /**
     * Check if the player has won
     *
     * @return bool
     */
    public function didPlayerWin(): bool
    {
        $player = $this->players[$this->currentPlayer];
        return $player->purse >= 6;
    }
}
