<?php

/**
 * Player class containing the purse and its place. Is being instantiated in Game
 */
class Player
{
    public $name;
    public $purse; // aka score
    public $place; // aka position
    public $inPenaltyBox; // boolean

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->purse = 0;
        $this->place = 0;
        $this->inPenaltyBox = false;
    }
}
